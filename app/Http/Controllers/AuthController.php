<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
        //dd($request->all());
        $nama = $request['nama'];
        return view('halaman.home', compact('nama'));
    }
}
