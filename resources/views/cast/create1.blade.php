@extends('layout.master')

@section('judul')
Tambah kategori
@endsection

@section('content')


            <form action="/cast" method="POST">
                @csrf
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="nama"  placeholder="Masukkan Title">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                <div class="form-group">
                    <label> Umur </label>
                    <input type="int" class="form-control" name="umur"  placeholder="Masukkan Title">
                    @error('umur')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                <div class="form-group">
                    <label>Bio</label>
                    <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
                    @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                <button type="submit" class="btn btn-primary mt-3">Tambah</button>
            </form>

@endsection