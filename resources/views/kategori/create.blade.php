@extends('layout.master')

@section('judul')
Tambah kategori
@endsection

@section('content')


            <form action="/kategori" method="POST">
                @csrf
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="nama"  placeholder="Masukkan Title">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                <button type="submit" class="btn btn-primary">Tambah</button>
            </form>

@endsection